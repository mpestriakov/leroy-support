// LOCAL DATA
let myData = [{
        "STORE": "1",
        "STORE_NAME": "Пенза",
        "VISIBLE": "0",
        "POSITION": "10",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": "1",
        "DATE_OPEN": "06-NOV-19",
        "DATE_RECEIVING": "06.11.19",
        "DATE_NEW_RECEIVING": "08.05.20 ",
        "DATE_AXA": "10.05.20 ",
        "DATE_NEW_AXA": "<span style=\"color: rgb(255,0,0);\">Включено в бюджет 2020.Ожидаемый срок выполнения работ - 1 квартал 2020.<\/span>\n\n",
        "DATE_BUILD": " 09.06.20",
        "DATE_NEW_BUILD": " 10.7.20",
        "STORE_ACCESS_ID": "67",
        "STATUS_AXA": "<span style=\"color: rgb(255, 153, 0);\">ОРАНЖЕВЫЙ<\/span>"
    },
    {
        "STORE": "2",
        "STORE_NAME": "Волжский",
        "VISIBLE": "0",
        "POSITION": "20",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": "2",
        "DATE_OPEN": "06-NOV-19",
        "DATE_RECEIVING": "06.11.19",
        "DATE_NEW_RECEIVING": " ",
        "DATE_AXA": " ",
        "DATE_NEW_AXA": " ",
        "DATE_BUILD": " ",
        "DATE_NEW_BUILD": " ",
        "STORE_ACCESS_ID": "68",
        "STATUS_AXA": " "
    },
    {
        "STORE": "3",
        "STORE_NAME": "Казань-2",
        "VISIBLE": "0",
        "POSITION": "30",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "69",
        "STATUS_AXA": null
    },
    {
        "STORE": "7",
        "STORE_NAME": "Ульяновск",
        "VISIBLE": "0",
        "POSITION": "70",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": "3",
        "DATE_OPEN": "06-NOV-19",
        "DATE_RECEIVING": "06.11.19",
        "DATE_NEW_RECEIVING": " ",
        "DATE_AXA": " ",
        "DATE_NEW_AXA": " ",
        "DATE_BUILD": " ",
        "DATE_NEW_BUILD": " ",
        "STORE_ACCESS_ID": "73",
        "STATUS_AXA": " "
    },
    {
        "STORE": "9",
        "STORE_NAME": "Тольятти",
        "VISIBLE": "0",
        "POSITION": "90",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "75",
        "STATUS_AXA": null
    },
    {
        "STORE": "13",
        "STORE_NAME": "Картмазово",
        "VISIBLE": "0",
        "POSITION": "130",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": "4",
        "DATE_OPEN": "16-NOV-19",
        "DATE_RECEIVING": "26.11.19",
        "DATE_NEW_RECEIVING": " ",
        "DATE_AXA": " ",
        "DATE_NEW_AXA": " ",
        "DATE_BUILD": " ",
        "DATE_NEW_BUILD": " ",
        "STORE_ACCESS_ID": "79",
        "STATUS_AXA": " "
    },
    {
        "STORE": "15",
        "STORE_NAME": "Ставрополь",
        "VISIBLE": "0",
        "POSITION": "150",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "81",
        "STATUS_AXA": null
    },
    {
        "STORE": "16",
        "STORE_NAME": "Оренбург",
        "VISIBLE": "0",
        "POSITION": "160",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "82",
        "STATUS_AXA": null
    },
    {
        "STORE": "21",
        "STORE_NAME": "Воронеж-2",
        "VISIBLE": "0",
        "POSITION": "210",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "87",
        "STATUS_AXA": null
    },
    {
        "STORE": "25",
        "STORE_NAME": "Тюмень Тобольский тракт",
        "VISIBLE": "0",
        "POSITION": "250",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "91",
        "STATUS_AXA": null
    },
    {
        "STORE": "26",
        "STORE_NAME": "Пермь. Ипподром Космонавтов",
        "VISIBLE": "0",
        "POSITION": "260",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "92",
        "STATUS_AXA": null
    },
    {
        "STORE": "28",
        "STORE_NAME": "Саранск",
        "VISIBLE": "0",
        "POSITION": "280",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "94",
        "STATUS_AXA": null
    },
    {
        "STORE": "32",
        "STORE_NAME": "Нижний Новгород",
        "VISIBLE": "0",
        "POSITION": "320",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "98",
        "STATUS_AXA": null
    },
    {
        "STORE": "42",
        "STORE_NAME": "Ростов-3",
        "VISIBLE": "0",
        "POSITION": "420",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "108",
        "STATUS_AXA": null
    },
    {
        "STORE": "46",
        "STORE_NAME": "Екатеринбург-2",
        "VISIBLE": "0",
        "POSITION": "460",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "112",
        "STATUS_AXA": null
    },
    {
        "STORE": "50",
        "STORE_NAME": "Адыгея Transfert",
        "VISIBLE": "0",
        "POSITION": "500",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "116",
        "STATUS_AXA": null
    },
    {
        "STORE": "53",
        "STORE_NAME": "Краснодар 3",
        "VISIBLE": "0",
        "POSITION": "530",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "119",
        "STATUS_AXA": null
    },
    {
        "STORE": "54",
        "STORE_NAME": "Новороссийск",
        "VISIBLE": "0",
        "POSITION": "540",
        "STORE_NUMBER": null,
        "STORE_TASK_ID": null,
        "DATE_OPEN": null,
        "DATE_RECEIVING": null,
        "DATE_NEW_RECEIVING": null,
        "DATE_AXA": null,
        "DATE_NEW_AXA": null,
        "DATE_BUILD": null,
        "DATE_NEW_BUILD": null,
        "STORE_ACCESS_ID": "120",
        "STATUS_AXA": null
    }
];

/* Formatting function for row details - modify as you need */
function format(d) {

    // `d` is the original data object for the row
    return '<table class="cell-border" style="width:100%">' +
        '<thead>' +
        '<tr>' +
        '<button class="editBtn" id ="' + d.STORE + '" >' + 'Edit' + '</button>' +
        '<th rowspan=2>' +
        'Магазин' +
        '</th>' +
        '<th rowspan=2>' +
        'Дата Открыта' +
        '</th>' +
        '<th colspan=2>' +
        'Приемка в эксплуатацию (технический отдел)' +
        '</th>' +
        '<th colspan=3>' +
        'Приемка AXA Matrix' +
        '</th>' +
        '<th colspan=2>' +
        'Строительный аудит' +
        '</th>' +
        '</tr>' +
        '<tr>' +
        '<th>' +
        'Дата устранения' +
        '</th>' +
        '<th>' +
        'Новая Дата' +
        '</th>' +
        '<th>' +
        'Статус' +
        '</th>' +
        '<th>' +
        'Дата устранения замечаний' +
        '</th>' +
        '<th>' +
        'Новая Дата' +
        '</th>' +
        '<th>' +
        'Дата устранения замечаний' +
        '</th>' +
        '<th>' +
        'Новая Дата' +
        '</th>' +


        '</tr>' +
        '</thead>' +

        '<tr>' +
        '<td>' + d.STORE_NAME + '</td>' +
        '<td>' + d.DATE_OPEN + '</td>' +
        '<td>' + d.DATE_RECEIVING + '</td>' +
        '<td>' + d.DATE_NEW_RECEIVING + '</td>' +
        '<td>' + d.STATUS_AXA + '</td>' +
        '<td>' + d.DATE_AXA + '</td>' +
        '<td>' + d.DATE_NEW_AXA + '</td>' +
        '<td>' + d.DATE_BUILD + '</td>' +
        '<td>' + d.DATE_NEW_BUILD + '</td>' +
        '</tr>' +

        '</table>';
}

$(document).ready(function () {
    let table,
        store,
        f = document.forms[0];

    table = $('#example').DataTable({
        // "ajax": "./arrays.json",
        data: myData,
        "paging": false,
        "drawCallback": function () {
            console.log('draw');
        },
        "columns": [

            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "STORE"
            },
            {
                "data": "STORE_NAME"
            },
            {
                "data": "DATE_OPEN"
            }
        ],
        "order": [
            [1, 'asc']
        ]
    });

    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
        let tr = $(this).closest('tr');
        let row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already opened - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    // Show modal box when click edit btn
    $('#example').on('click', '.editBtn', function () {
        showModal(this);
    });

    // Close modal box when click cross btn
    $('.modal-close').click(() => {
        $('.modal').hide();
    });

    // Update the data 
    $('.modal-update-btn').click((event) => {
        event.preventDefault();

        // Set the data
        store.STORE_NAME = f.store_name.value;
        store.DATE_OPEN = f.date_open.value;
        store.DATE_RECEIVING = f.date_receiving.value;
        store.DATE_NEW_RECEIVING = f.date_new_receiving.value;
        store.STATUS_AXA = f.status.value;
        store.DATE_AXA = f.date_axa.value;
        store.DATE_NEW_AXA = f.date_new_axa.value;
        store.DATE_BUILD = f.date_build.value;
        store.DATE_NEW_BUILD = f.date_new_build.value;
        $('.modal').hide();

        table.draw('full-hold');
    })

    function showModal(target) {
        $('.modal').show();

        // Current store
        store = myData.find((el) => el.STORE == target.id);
        currentTr = target.closest('tr');
        currentRow = table.row(currentTr);

        // Get the data
        f.store_name.value = store.STORE_NAME;
        f.date_open.value = store.DATE_OPEN;
        f.date_receiving.value = store.DATE_RECEIVING;
        f.date_new_receiving.value = store.DATE_NEW_RECEIVING;
        f.status.value = store.STATUS_AXA;
        f.date_axa.value = store.DATE_AXA;
        f.date_new_axa.value = store.DATE_NEW_AXA;
        f.date_build.value = store.DATE_BUILD;
        f.date_new_build.value = store.DATE_NEW_BUILD;
    }
});

// Close modal if you click somewhere 
window.onclick = event => {
    if (event.target == document.querySelector('.modal')) {
        document.querySelector('.modal').style.display = 'none';
    }
}